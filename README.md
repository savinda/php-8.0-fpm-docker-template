# php-8.0-fpm-docker-template


## Docker Exec Usage
```
# composer install
docker compose exec --user=laravel app composer install

# running artisan tinker
docker compose exec --user=laravel app php artisan tinker

# run migrations
docker compose exec --user=laravel app php artisan migrate
docker compose exec --user=laravel app php artisan migrate:rollback

# accessing container
docker compose exec --user=laravel app bash
```