FROM php:8.0-fpm

# Arguments defined in docker-compose.yml
ENV USER=laravel
ENV UID=1000
ENV PWD=/var/www/html

# Install system dependencies
RUN apt-get update && apt-get install -y \
    git \
    curl \
    libpq5 \
    libpq-dev \
    libonig-dev \
    libxml2-dev \
    zip \
    unzip \
    supervisor \
    nginx

# Install PHP extensions
RUN docker-php-ext-install pdo pdo_pgsql mbstring exif pcntl bcmath 

# Clear cache
RUN apt-get clean && \
    apt-get autoremove --purge -y libpq-dev && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /usr/share/doc/*

# Get latest Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

# Use local configuration
COPY docker/supervisor.conf.tpl /etc/supervisor/conf.d/supervisor.conf

# Configure nginx
COPY docker/nginx.conf /etc/nginx/nginx.conf

# Copy php.ini config
COPY docker/www.conf /usr/local/etc/php-fpm.d/zz-docker.conf

# Copy scripts
COPY docker/init.sh /usr/local/bin/init.sh

## Create system user to run Composer and Artisan Commands
RUN useradd -G www-data,root -u $UID -d /home/$USER $USER
RUN mkdir -p /home/$USER/.composer && \
    chown -R $USER:$USER /home/$USER

# Set working directory
WORKDIR $PWD

# Run supervisor
ENTRYPOINT ["/bin/sh", "/usr/local/bin/init.sh"]

# Configure a healthcheck to validate that everything is up&running
HEALTHCHECK --timeout=10s CMD curl --silent --fail http://127.0.0.1:80/fpm-ping
